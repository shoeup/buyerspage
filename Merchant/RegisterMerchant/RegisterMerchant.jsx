import React, { useState } from "react";

import { Navigate } from "react-router-dom";
import "../../Buyer/RegisterUser/Register.css";
import image from '../../LoginScreen/LogoShoeUp.png'
import Footer from "../../merchantFolder/merchant/footer/Footer/Footer";
function RegisterMerchant() {
  const [name, setName] = useState("");
  //   const [username,setUsername]=useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [islogin, setIslogin] = useState(false);

  function emailChangeHandler(e) {
    setEmail(e.target.value);
  }

  function passwordChangeHandler(e) {
    setPassword(e.target.value);
  }
  function nameChangeHandler(e) {
    setName(e.target.value);
  }

  const roleType = 2;
  function onSubmit(e) {
    e.preventDefault();
    const data = {
      name,
      email,
      password,
      roleType,
    };
    fetch("http://172.16.28.149:8080/add", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
      },
    })
      .then((res) => {
        if (res.status === 2000 || res.status === 202) {
          setIslogin(true);

          // console.log("DONE SUCCESS")
        }
      })
      .catch((e) => {
        console.error("Something wrong");
      });
  }

  return (
    <div>
    <div>
      <header className="loginHeader">
        <div class="registerLoginContainer">
        <img className="logoLogin" src={image}/>
        </div>
      </header>
      <form className="registerForm" onSubmit={onSubmit}>
        <h1>Merchant Registration</h1>

        <input
          type="text"
          onChange={nameChangeHandler}
          name="name"
          value={name}
          placeholder="Name"
          required
        />

        <input
          type="email"
          onChange={emailChangeHandler}
          name="email"
          value={email}
          placeholder="Email"
          required
        />

        <input
          type="password"
          onChange={passwordChangeHandler}
          name="password"
          value={password}
          placeholder="Password"
          required
        />

        <input className="loginButton" type="submit" value="Register" />
      </form>
      {/* {console.log(islogin)} */}
      {islogin && alert("Registered as Merchant")}
      {islogin && <Navigate replace to="/login" /> }
    </div>
    <Footer/>
    </div>
  );
}

export default RegisterMerchant;
