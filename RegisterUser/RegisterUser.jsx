import React, { useState } from "react";

import { Link, Navigate } from "react-router-dom";
import "./Register.css";
import image from "../../LoginScreen/LogoShoeUp.png"
import Footer from "../../merchantFolder/merchant/footer/Footer/Footer";
function RegisterUser() {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  const [islogin, setIslogin] = useState(false);
  function emailChangeHandler(e) {
    setEmail(e.target.value);
  }

  function passwordChangeHandler(e) {
    setPassword(e.target.value);
  }
  function nameChangeHandler(e) {
    setName(e.target.value);
  }

  const roleType = 1;
  function onSubmit(e) {
    e.preventDefault();
    const data = {
      name,
      email,
      password,
      roleType,
    };
    fetch("http://172.16.28.149:8080/add", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
      },
    })
      .then((res) => {
        if (res.status === 2000 || res.status === 202) {
          setIslogin(true);

          // console.log("DONE SUCCESS")
        }
      })
      .catch((e) => {
        console.error("Something wrong");
      });
  }

  return (
    <div>
    <div>
      <header>
        <div class="registerLoginContainer">
        <img className="logoLogin" src={image}/>
        </div>
      </header>
      <form className="registerForm" onSubmit={onSubmit}>
        <h1>Register</h1>

        <input
          type="text"
          onChange={nameChangeHandler}
          name="name"
          value={name}
          placeholder="Name"
          required
        />

        <input
          type="email"
          onChange={emailChangeHandler}
          name="email"
          value={email}
          placeholder="Email"
          required
        />

        <input
          type="password"
          onChange={passwordChangeHandler}
          name="password"
          value={password}
          placeholder="Password"
          required
        />

        <input className="loginButton" type="submit" value="Register" />

        <Link to="/registerMerchant" className="registerScreen_link">
          Register as Merchant?
        </Link>
      </form>
      {islogin && alert("Registered successfully")}
      {islogin && <Navigate replace to="/login" />}
    </div>
    <Footer/>
    </div>
  );
}

export default RegisterUser;
