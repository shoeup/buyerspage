import React, { useState } from "react";
import "./Login.css";
import "../../App.css";
import "../Buyer/RegisterUser/Register.css";

import image from "./LogoShoeUp.png";

import { Link, Navigate, useNavigate } from "react-router-dom";
import Footer from "../merchantFolder/merchant/footer/Footer/Footer";
function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [islogin, setIslogin] = useState(false);
  const [sessionValue, setSessionValue] = useState({});
  const navigate = useNavigate();
  function emailChangeHandler(e) {
    setEmail(e.target.value);
  }

  function passwordChangeHandler(e) {
    setPassword(e.target.value);
  }

  function onSubmit(e) {
    e.preventDefault();
    const data = {
      email,
      password,
    };
    fetch("http://172.16.28.149:8080/login", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
      },
    })
      .then((res) => {
        if (res.status === 200 || res.status === 202) {
          setIslogin(true);
          if (sessionValue.id === null) {
            alert("invalid credentials");
          }
          return res.json();
        }
      })
      .then((data) => {
        console.log(data);

        setSessionValue(data);

        localStorage.setItem("token", data.token);
        localStorage.setItem("email", data.email);
        localStorage.setItem("id", data.id);
      })
      .catch((e) => {
        console.error("Something wrong");
      });
  }

  return (
    <div>
      <div className="registerLoginContainer">
        <header className="loginHeader">
          {/* <h1 className="company-name">shoeUp</h1> */}
          <img className="logoLogin" src={image} />
        </header>

        <div>
          <div className="signinScreen ">
            <form className="registerForm" onSubmit={onSubmit}>
              <h1 >Sign In</h1>
              <input
                className="fields"
                onChange={emailChangeHandler}
                placeholder="Email"
                type="email"
                value={email}
                required
              />
              <input
                className="fields"
                onChange={passwordChangeHandler}
                placeholder="Password"
                type="password"
                value={password}
                required
              />
              <input className="loginButton" type="submit" value="Sign In" />

              <h4>
                <span className="signinScreen_gray">New to shoeUp?</span>
                <Link to="/registerUser" className="signinScreen_link">
                  Register now.
                </Link>
              </h4>
            </form>
          </div>
        </div>
        {islogin &&
          sessionValue.role === 1 &&
          navigate(`/shoeUpUser/${sessionValue.id}`)}

        {islogin &&
          sessionValue.role === 2 &&
          navigate(`/land-to-merchant/${sessionValue.id}`)}
      </div>
      <Footer />
    </div>
  );
}

export default Login;
