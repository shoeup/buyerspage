import React from "react";
import "./producthistory.css";
function Producthistroy() {
  return (
    <div>
      <div className="productcontainer3">
        <div>
          <img
            id="productimg3"
            src="https://images.unsplash.com/photo-1542291026-7eec264c27ff?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80"
            alt="name"
          />
        </div>

        <div>
          <h1>ProductName</h1>
          <br />
          <h2 id="price3">price:$800</h2>
          <p id="date3">Date</p>
        </div>
      </div>
    </div>
  );
}

export default Producthistroy;
