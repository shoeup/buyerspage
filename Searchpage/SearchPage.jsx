import React, { useEffect } from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";

import Navbar from "../NavbarBuyer/navbar/Navbar";
import Productsearch from "./ProductSearch/Productsearch";

function Searchpage() {
  const [productsearch, setproductsearch] = useState([]);
  let { firstName } = useParams();
  console.log(firstName);
  useEffect(function () {
    fetch(`http://172.16.29.152:8080/getproduct?productName=${firstName}`, {
      method: "GET",
    })
      .then((res) => {
        console.log("GOT Response", res);
        if (res.status === 200 || res.status === 202) {
          console.log(res);
          return res.json();
        }
        return res;
      })
      .then((res) => {
        setproductsearch(res);
      })
      .catch((e) => {
        console.error("Something Failed Here", e);
      });
  }, []);

  return (
    <div>
      <Navbar />
      <div className="productlanding">
        {productsearch &&
          productsearch.map(function (product) {
            return <Productsearch product={product} />;
          })}
      </div>
    </div>
  );
}

export default Searchpage;
