import React from "react";
import "./productsearch.css";
import { useNavigate } from "react-router-dom";
function Productsearch(props) {
  const navigate = useNavigate();
  const {
    id,
    category_Id,
    stock,
    price,
    color,
    staticId,
    productName,
    description,
    productImage,
  } = props?.product || {};

  return (
    <div
      onClick={function () {
        navigate(`/productDetails/${staticId}`);
      }}
      className="productcontainer5"
    >
      <div>
        <img id="productimg5" src={productImage} alt="name" />
      </div>

      <div>
        <h1>{productName}</h1>
        <br />
        <p>{description}</p>
        <br />
        <h2 id="price5">Price: ${price}</h2>
      </div>
    </div>
  );
}

export default Productsearch;
