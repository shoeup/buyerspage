import Productcart from "./productcart/Productcart";
import React, { useEffect } from "react";
import { useState } from "react";
import Navbar from "../NavbarBuyer/navbar/Navbar";
import "./Cartpage.css";

function Cartpage() {
  const [cart, setcart] = useState([]);
  const uId = localStorage.getItem("id");
  useEffect(function () {
    fetch(`http://172.16.28.120:8080/cartview?id=${uId}`, {
      method: "GET",
    })
      .then((res) => {
        console.log("GOT Response", res);
        if (res.status === 200 || res.status === 202) {
          console.log(res);
          return res.json();
        }
        return res;
      })
      .then((res) => {
        setcart(res);
      })
      .catch((e) => {
        console.error("Something Failed Here", e);
      });
  }, []);

  return (
    <div>
      <Navbar />
      <div className="cartProd">
        {cart &&
          cart.map(function (product) {
            return <Productcart product={product} />;
          })}
      </div>
    </div>
  );
}

export default Cartpage;
