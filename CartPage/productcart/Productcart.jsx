import React from "react";
import { useState } from "react";
import "./productcart.css";
import { useNavigate } from "react-router-dom";
import Buycart from "../buyincart/Buycart";
import Removeincart from "../removeincart/Removeincart";
function Productcart(props) {
  const [quantity, setquantity] = useState("");
  const onQunatityChange = function (e) {
    const value = e.target.value;
    setquantity(value);
  };
  const navigate = useNavigate();
  const {
    static_Id,
    dynamicId,
    categoryId,
    productName,
    productImage,
    description,
    merchantId,
    stock,
    price,
    size,
  } = props?.product || {};

  //   props.cartTotalFunction(price, 2);
  return (
    <div className="contan1">
      <div
        className="productcontainer1"
        onClick={function () {
          navigate(`/productDetails/${dynamicId}`);
        }}
      >
        <div>
          <img id="productimg1" src={productImage} alt="name" />
        </div>

        <div>
          <h1>{productName}</h1>
          <br />
          {/* <p>Merchant_Name</p> */}
          <p>{description}</p>
          <h2 id="price1">Price:₹{price}</h2>
          <p id="stock1">Stock:{stock}</p>
        </div>
      </div>
      <div id="quantity">
        <label for="quantity">Quantity</label>
        <input
          type="number"
          name="quantity"
          style={{ paddingLeft: "15px" }}
          min="1"
          max="5"
          defaultValue="1"
          onChange={onQunatityChange}
        />
        <Removeincart />
        <Buycart dynamicId={dynamicId} price={price} quantity={quantity} />
      </div>
    </div>
  );
}

export default Productcart;
