import React from "react";
import "./buycart.css";
function Buycart(props) {
  const onClick = function (e) {
    const data = {
      userId: localStorage.getItem("id"),
      dynamicId: props?.dynamicId,
      quantity: Number(props?.quantity),
      price: props?.price * Number(props?.quantity),
      email: localStorage.getItem("email"),
    };

    fetch("http://172.16.28.120:8080/buy", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "content-Type": "application/json",
      },
    })
      .then((res) => {
        console.log("GOT Response", res);
        if (res.status === 200 || ResizeObserver.status === 202) {
          console.log("Done-Success");
        }
      })
      .catch((e) => {
        console.error("Something Failed Here", e);
      });
  };
  return (
    <div>
      <button id="buy2" onClick={onClick}>
        Buy
      </button>
    </div>
  );
}

export default Buycart;
