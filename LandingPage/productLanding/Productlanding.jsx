import React from "react";
import "./productlanding.css";
import { useNavigate } from "react-router-dom";
function Productlanding(props) {
  const navigate = useNavigate();
  const {
    stock,
    price,
    color,
    staticId,
    dynamicId,
    categoryId,
    productName,
    description,
    productImage,
  } = props?.product || {};

  return (
    <div
      onClick={function () {
        navigate(`/productDetails/${staticId}`);
      }}
      className="productcontainer"
    >
      <div>
        <img id="productimg" src={productImage} alt="name" />
      </div>

      <div>
        <h1>{productName}</h1>

        <br />
        <h3>{description}</h3>
        <br />
        <h2 id="price">Price: ₹{price}</h2>
      </div>
    </div>
  );
}

export default Productlanding;
