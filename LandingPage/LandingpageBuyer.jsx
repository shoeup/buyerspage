import React, { useEffect } from "react";
import { useState } from "react";
import Productlanding from "./productLanding/Productlanding";
import "./landingpageBuyer.css";
import Navbar from "../NavbarBuyer/navbar/Navbar";

function LandingpageBuyer() {
  const [productlist, setproductlist] = useState([]);
  useEffect(function () {
    fetch("http://172.16.28.120:8080/home", {
      method: "GET",
    })
      .then((res) => {
        console.log("GOT Response", res);
        if (res.status === 200 || res.status === 202) {
          console.log(res);
          return res.json();
        }
        return res;
      })
      .then((res) => {
        setproductlist(res);
      })
      .catch((e) => {
        console.error("Something Failed Here", e);
      });
  }, []);

  return (
    <div>
      <Navbar />
      <div className="productlanding">
        {productlist &&
          productlist.map(function (product) {
            return <Productlanding product={product} />;
          })}
      </div>
    </div>
  );
}

export default LandingpageBuyer;
