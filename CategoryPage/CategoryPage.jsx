import React, { useEffect } from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";

import Navbar from "../NavbarBuyer/navbar/Navbar";
import Productcategory from "./Productcategory/Productcategory";

function CategoryPage() {
  const [productcat, setproductcat] = useState([]);
  let { id } = useParams();
  useEffect(function () {
    fetch(`http://172.16.28.120:8080/category?categoryId=${id}`, {
      method: "GET",
    })
      .then((res) => {
        console.log("GOT Response", res);
        if (res.status === 200 || res.status === 202) {
          console.log(res);
          return res.json();
        }
        return res;
      })
      .then((res) => {
        setproductcat(res);
      })
      .catch((e) => {
        console.error("Something Failed Here", e);
      });
  });

  return (
    <div>
      <Navbar />
      <div className="productlanding">
        {productcat &&
          productcat.map(function (product) {
            return <Productcategory product={product} />;
          })}
      </div>
    </div>
  );
}

export default CategoryPage;
