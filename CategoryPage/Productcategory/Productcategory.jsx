import React from "react";
import "./productcategory.css";
import { useNavigate } from "react-router-dom";
function Productcategory(props) {
  const navigate = useNavigate();
  const {
    price,
    stock,
    color,
    static_Id,
    category_Id,
    product_Name,
    description,
    product_Image,
  } = props?.product || {};

  return (
    <div
      onClick={function () {
        navigate(`/productDetails/${static_Id}`);
      }}
      className="productcontainer6"
    >
      <div>
        <img id="productimg6" src={product_Image} alt="name" />
      </div>

      <div>
        <h1>{product_Name}</h1>
        <br />
        <p>Merchant Name</p>
        <br />
        <p>{description}</p>
        <br />
        <h2 id="price6">Price: ₹{price}</h2>
      </div>
    </div>
  );
}

export default Productcategory;
