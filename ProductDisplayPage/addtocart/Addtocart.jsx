import React from "react";
import "./addtocart.css";
function Addtocart(props) {
  const onClick = function (e) {
    const data = {
      productDynamicId: props?.productDynamicId,
      userId: localStorage.getItem("id"),
      quantity: 1,
      price: props?.price,
    };

    fetch("http://172.16.28.120:8080/addtocart", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "content-Type": "application/json",
      },
    })
      .then((res) => {
        console.log("GOT Response", res);
        if (res.status === 200 || ResizeObserver.status === 202) {
          console.log("Done-Success");
        }
      })
      .catch((e) => {
        console.error("Something Failed Here", e);
      });
  };
  return (
    <div>
      <button id="addtocart" onClick={onClick}>
        Add to Cart
      </button>
    </div>
  );
}

export default Addtocart;
