import { React, useEffect, useState } from "react";
import Addtocartotherseller from "./addtocartofotherseller/Addtocartotherseller";

import "./otherseller.css";
const Otherseller = (props) => {
  const { id, staticId, merchantId, stock, price, size } = props?.product || {};
  const [othersellername, setothersellername] = useState("");
  useEffect(function () {
    fetch(`http://172.16.28.120:8080/merchant?id=${merchantId}`, {
      method: "GET",
    })
      .then((res) => {
        console.log("GOT Response", res);
        if (res.status === 200 || res.status === 202) {
          console.log(res);
          return res.json();
        }
        return res;
      })
      .then((res) => {
        setothersellername(res);
      })
      .catch((e) => {
        console.error("Something Failed Here", e);
      });
  }, []);

  const {
    id: pid,
    userId,
    gst,
    merchantRating,
    color,
    name,
  } = othersellername || {};
  // localStorage.getItem()
  return (
    <div>
      <div className="othersellers">
        <p>{name}</p>
        <p>MerchantRating: {merchantRating}</p>
        <p>GST: {gst}</p>
        <p id="price1">price:{price}</p>

        <Addtocartotherseller productDynamicId={id} price={price} />
      </div>
    </div>
  );
};

export default Otherseller;
