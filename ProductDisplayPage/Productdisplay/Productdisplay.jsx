import React, { useEffect } from "react";
import { useState } from "react";
import Addtocart from "../addtocart/Addtocart";
import Buy from "../buy/Buy";
import Otherseller from "../otherseller/Otherseller";
import "./productdisplay.css";
import { useParams } from "react-router-dom";
import Navbar from "../../NavbarBuyer/navbar/Navbar";
function Productdisplay() {
  const [product, setproduct] = useState("");
  let { id } = useParams();
  useEffect(function () {
    fetch(`http://172.16.28.120:8080/product?id=${id}`, {
      method: "GET",
    })
      .then((res) => {
        console.log("GOT Response", res);
        if (res.status === 200 || res.status === 202) {
          console.log(res);
          return res.json();
        }
        return res;
      })
      .then((res) => {
        setproduct(res);
      })
      .catch((e) => {
        console.error("Something Failed Here", e);
      });
  }, []);
  const {
    id: pid,
    categoryId,
    productName,
    productImage,
    color,
    productDynamicList,
    description,
    price,
  } = product || {};
  return (
    <div>
      <Navbar />
      <div className="contain">
        <div className="productconta">
          <div>
            <img id="productimg" src={productImage} alt="name" />
          </div>
          <div>
            <h1 id="productname">{productName}</h1>
            <br />
            <h3>{description}</h3>
            {/* <h2 id="price">Price :₹{price}</h2> */}
            {/* <p id="stock">Stock:8</p> */}
          </div>
        </div>
        <div id="operation3">
          {/* <Buy /> */}
          {/* <Addtocart productDynamicId={pid} price={price} /> */}
        </div>
        <br />
      </div>
      <div className="othersellerbar">Sellers</div>
      <div className="otherselleritems">
        {productDynamicList &&
          productDynamicList.map((product) => {
            return <Otherseller product={product} />;
          })}
      </div>
    </div>
  );
}

export default Productdisplay;
