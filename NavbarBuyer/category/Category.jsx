import { useNavigate } from "react-router-dom";
import React, { useState } from "react";
function Category() {
  const [selected, setSelected] = useState([]);
  const navigate = useNavigate();
  const selectionChangeHandler = (event) => {
    setSelected(event.target.value);
  };
  return (
    <div>
      <div class="dropdown">
        <button class="dropbtn">Category</button>
        <div class="dropdown-content">
          <a
            onClick={function () {
              navigate("/categoryPage/1");
            }}
            href="#"
          >
            Casuals
          </a>
          <a
            onClick={function () {
              navigate("/categoryPage/2");
            }}
            href="#"
          >
            Formal
          </a>
          <a
            onClick={function () {
              navigate("/categoryPage/3");
            }}
            href="#"
          >
            Sports
          </a>
          <a
            onClick={function () {
              navigate("/categoryPage/4");
            }}
            href="#"
          >
            Sneakers
          </a>
        </div>
      </div>
    </div>
  );
}

export default Category;
