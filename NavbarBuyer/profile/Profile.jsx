import React from "react";
import "./profile.css";
import { useNavigate } from "react-router-dom";

function Profile() {
  // const navigate = useNavigate();
  // function logoutHandler() {
  //   localStorage.clear();
  //   navigate("/");
  // }
  return (
    <div class="dropdown">
      <button class="dropbtn">
        <img
          className="profileIcon"
          src="https://img.freepik.com/premium-vector/man-avatar-profile-round-icon_24640-14044.jpg?w=2000"
          alt=""
          srcset=""
        />
      </button>
      <div class="dropdown-content">
        <a href="#">Order History</a>
        <a href="#"> Logout</a>
        {/* onClick={logoutHandler} */}
      </div>
    </div>
  );
}

export default Profile;
