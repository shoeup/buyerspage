import React from "react";
import SearchBar from "../searchbar/SearchBar";
import Category from "../category/Category";
import { Link, Navigate } from "react-router-dom";
import Profile from "../profile/Profile";
import "./navbar.css";
import image from "./logo.png";

import image4 from "./logo4.png";
function Navbar() {
  return (
    <div>
      <div className="navWrapper">
        <Link to="/shoeUpUser" className="brandName">
          <img src={image4} alt="" srcset="" />
        </Link>

        <div className="cartWrapper">
          <SearchBar />
          <div className="category">
            <Category />
          </div>

          <Link to="/cart" className="cart">
            <span>2</span>
            <img
              // src="https://www.freeiconspng.com/uploads/shopping-cart-icon-30.png"
              src="https://cdn.imgbin.com/21/3/17/imgbin-shopping-cart-computer-icons-white-cart-simple-white-shopping-cart-illustration-nDJVn3nE6gpyZGMq8spPi2dy0.jpg"
              className="cartIcon"
              alt=""
              srcset=""
            />
          </Link>
          <div>
            <Profile />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
