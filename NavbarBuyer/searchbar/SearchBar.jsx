import React from "react";
import "./SearchBar.css";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

function SearchBar() {
  const navigate = useNavigate();
  const [firstName, setFirstname] = useState("");
  const onFirstNameChange = function (e) {
    const value = e.target.value;
    setFirstname(value);
  };

  function onSubmit(e) {
    e.preventDefault();
    navigate(`/searchPage/${firstName}`);
  }

  return (
    <div>
      <form onSubmit={onSubmit} className="searchForm">
        <input
          className="globalSearch"
          placeholder="Search Items"
          type="search"
          name="search"
          onChange={onFirstNameChange}
          // onChange={onChangeHandler}
        />
        <input id="searchbtn" type="submit" value="search" />
      </form>
    </div>
  );
}

export default SearchBar;
